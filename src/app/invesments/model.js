import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const invesmentSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  desc: {
    type: String,
    required: true
  },
  investors: {
    type: Schema.Types.Mixed,
    default: null
  },
  created: {
    type: Date,
    default: Date.now
  },
  updated: {
    type: Date,
    default: Date.now
  },
  create_by: {
    type: Schema.Types.Mixed,
    default: null
  }
});

// todoSchema.methods.generateSlug = function() {
//   this.slug = `${this.title.toLowerCase().replace(/ +/g, "-")}-${this.id}`;
// };

// todoSchema.statics.findOneBySlug = function(slug) {
//   return Todo.findOne({ slug });
// };

const Invesment = mongoose.model('Invesment', invesmentSchema);

export default Invesment;
