import { check } from "express-validator/check";
import passport from "passport";

import controller from "./controller";

const authJwt = () => passport.authenticate("jwt", { session: false });

const CreateValidators = [
  check("title")
    .isLength({ min: 1, max: 50 })
    .withMessage("must contain 1 - 50 chars long"),
  check("desc")
    .isLength({ min: 1 })
    .withMessage("must be at least 1 char")
];

export function setup(router) {
  router
    .get("/:id", [authJwt()], controller.getInvesment)
    .post("/", [authJwt(), CreateValidators], controller.creteInvesment)
    .post("/:id/investor", authJwt(), controller.createInvestor)
    .delete("/:id/investor", authJwt(), controller.deleteInvestor)
    .post("/invesments/:id/role", authJwt(), controller.updateRolveInvertor);
}
