import { validationResult } from 'express-validator/check'

import Investment from './model'
// import InvestmentPolicy from './policy'
import InvestmentSerializer from './serializer'
// import { Controller } from '@common'

const getInvesment = (req, res) => {
  Investment.findOne({ _id: req.params.id }).then(investments =>
    res.json({ investments: InvestmentSerializer.for('index', investments) })
  )
}

const creteInvesment = (req, res) => {
  const { title, desc } = req.body
  const Investment = new Investment({
    title,
    desc,
    investors: [],
    userId: req.user.id
  })
  Investment.save().then(invesment => res.status(201).json(invesment))
}

const createInvestor = (req, res) => {}

const deleteInvestor = (req, res) => {}

const updateRolveInvertor = (req, res) => {}

export default {
  getInvesment,
  creteInvesment,
  createInvestor,
  deleteInvestor,
  updateRolveInvertor
}
