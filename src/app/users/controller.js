import passport from "passport";

import UserSerializer from "./serializer";

const signup = (req, res) => {
  passport.authenticate(
    "local-signup",
    {
      session: false,
      failWithError: true
    },
    (_, user, info) => {
      if (info) {
        return res.status(401).json(info);
      }

      res.status(201).json({ user: UserSerializer.for("signup", user) });
    }
  )(req, res);
};

const signin = (req, res) => {
  passport.authenticate(
    "local-login",
    {
      session: false,
      failWithError: true
    },
    (_, user, info) => {
      if (info) {
        return res.status(401).json(info);
      }

      res.status(201).json({ user: UserSerializer.for("signin", user) });
    }
  )(req, res);
};

export default { signup, signin };
