import { validationResult } from 'express-validator/check'

import Todo from './model'
import TodoPolicy from './policy'
import TodoSerializer from './serializer'
import { Controller } from '@common'

const index = (_, res) => {
  Todo.find().then(todos =>
    res.json({ todos: TodoSerializer.for('index', todos) })
  )
}

const create = (req, res) => {
  if (!TodoPolicy.for('create', req.user)) {
    return Controller.throwMethodNotAllowed('todo', res)
  }

  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return Controller.throwUnprocessableEntity('todo', errors, res)
  }

  const { title, desc } = req.body
  const todo = new Todo({ title, desc, userId: req.user.id })

  todo.generateSlug()

  todo
    .save()
    .then(todo =>
      res.status(201).json({ todo: TodoSerializer.for('create', todo) })
    )
}

const update = (req, res) => {
  if (!TodoPolicy.for('update', req.user)) {
    return Controller.throwMethodNotAllowed('todo', res)
  }

  const errors = validationResult(req)

  if (!errors.isEmpty()) {
    return Controller.throwUnprocessableEntity('todo', errors, res)
  }

  Todo.findOneBySlug(req.params.slug)
    .then(todo => {
      const { title, desc } = req.body

      todo.title = title
      todo.desc = desc
      todo.generateSlug()

      return todo.save()
    })
    .then(todo => res.json({ todo: TodoSerializer.for('update', todo) }))
}

const destroy = (req, res) => {
  if (!TodoPolicy.for('delete', req.user)) {
    return Controller.throwMethodNotAllowed('todo', res)
  }

  Todo.deleteOne({ slug: req.params.slug }).then(() => res.status(204).send())
}

const show = (req, res) => {
  Todo.findOneBySlug(req.params.slug).then(todo =>
    res.json({ todo: TodoSerializer.for('show', todo) })
  )
}

export default { index, create, show, update, destroy }
