import { Policy } from '@common'

export default {
  ...Policy,

  create(user) {
    return !!user
  },

  build(user) {
    return !!user
  },

  update(user, todo) {
    return user && user.id === todo.userId
  },

  destroy(user, todo) {
    return user && user.id === todo.userId
  }
}
