import passport from 'passport'
import { Strategy as LocalStrategy } from 'passport-local'
import { Strategy as JWTStrategy, ExtractJwt } from 'passport-jwt'

import config from './config'
import User from '../app/users/model'

export default app => {
  app.use(passport.initialize())

  passport.serializeUser((user, done) => {
    done(null, user.id)
  })

  passport.deserializeUser((id, done) => {
    User.findById(id)
      .then(user => done(null, user))
      .catch(err => done(err))
  })

  passport.use(
    'local-signup',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
      },
      (req, email, password, done) => {
        User.findOne({ 'local.email': email })
          .then(user => {
            if (user) {
              return done(null, false, {
                error: 'That email is already taken.'
              })
            }

            const newUser = new User()

            newUser.local.email = email
            newUser.name = req.body.name
            newUser.local.password = newUser.encryptPassword(password)

            newUser.save().then(user => done(null, user))
          })
          .catch(err => done(err))
      }
    )
  )

  passport.use(
    'local-login',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password'
      },
      (email, password, done) => {
        User.findOne({ 'local.email': email })
          .then(user => {
            const isInvalid = !(user && user.validPassword(password))

            if (isInvalid) {
              return done(null, false, {
                error: 'Oops! Invalid Credentials.'
              })
            }

            return done(null, user)
          })
          .catch(err => done(err))
      }
    )
  )

  passport.use(
    new JWTStrategy(
      {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: config.secret
      },
      function(jwtPayload, done) {
        return User.findById(jwtPayload.sub)
          .then(user => {
            return done(null, user)
          })
          .catch(err => {
            return done(err)
          })
      }
    )
  )
}
