import mongoose from 'mongoose'

import config from './config'

export default function connectDatabase() {
  return mongoose.connect(
    config.databaseUrl,
    { useNewUrlParser: true }
  )
}
